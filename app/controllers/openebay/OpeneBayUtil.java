package controllers.openebay;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import play.Play;
import play.libs.WS;

/**
 * 
 * @author @kumaresan
 * 
 */
public class OpeneBayUtil {

	public static String sandboxPublicKey = null;
	public static String productionPublicKey = null;

	public static String getPublicKey(String env) {
		if ("sandbox".equals(env)) {
			return getSandboxPublicKey();
		}
		return getProductionPublicKey();
	}

	public static boolean verifyeBaySignature(String encodedSignature,
			String verificationString, String env) throws Exception {
		String cert = getPublicKey(env);
		return verifySignature(encodedSignature, verificationString, cert);
	}

	public static String getSandboxPublicKey() {
		if (sandboxPublicKey == null) {
			sandboxPublicKey = WS.url(
					"http://developer.ebay.com/certificates/sandbox.cert")
					.get().getString();

		}
		// System.out.println("OpeneBayRequest.getSandboxPublicKey() "
		// + sandboxPublicKey);
		return sandboxPublicKey.trim();
	}

	public static String getProductionPublicKey() {
		if (productionPublicKey == null) {
			productionPublicKey = WS.url(
					"http://developer.ebay.com/certificates/production.cert")
					.get().getString();

		}
		// System.out.println("OpeneBayRequest.getProductionPublicKey() "
		// + productionPublicKey);
		return productionPublicKey.trim();
	}

	public static String getDevID() {
		return Play.configuration
				.getProperty(OpeneBayApplication.OPENEBAY_DEV_ID);
	}

	public static String getAppID() {
		return Play.configuration
				.getProperty(OpeneBayApplication.OPENEBAY_APP_ID);
	}

	public static String getCertID() {
		return Play.configuration
				.getProperty(OpeneBayApplication.OPENEBAY_CERT_ID);
	}

	public static boolean verifySignature(String encodedSignature,
			String verificationString, String cert)
			throws java.security.cert.CertificateException,
			java.security.NoSuchAlgorithmException,
			java.security.InvalidKeyException,
			java.security.SignatureException, IOException {

		// create an X509 certificate
		CertificateFactory certificateFactory = CertificateFactory
				.getInstance("X.509");
		Certificate certificate = certificateFactory
				.generateCertificate(new ByteArrayInputStream(cert.getBytes()));

		// get the public key from the certificate
		PublicKey publicKey = certificate.getPublicKey();

		// verify the signature using SHA1withRSA
		Signature signatureRSA = Signature.getInstance("SHA1withRSA");
		signatureRSA.initVerify(publicKey);
		signatureRSA.update(verificationString.getBytes());

		byte[] signature = Base64.decodeBase64(encodedSignature.getBytes());

		boolean verified = signatureRSA.verify(signature);

		// it is true or false...
		return verified;
	}

	public static Map<String, String> getDecryptedeBayParameters(
			String encodedMessage) throws Exception {
		String certID = getCertID();
		String decryptedMessage = decrypt(encodedMessage, certID);

		Map<String, String> mapParams = new HashMap<String, String>();
		String[] params = decryptedMessage.split("&");

		for (String string : params) {
			String[] temp = string.split("=");
			mapParams.put(temp[0], URLDecoder.decode(temp[1], "UTF-8"));
		}

		return mapParams;
	}

	/** Decrypts the encodedMessage. */
	public static String decrypt(String encodedMessage, String masterKeyStr)
			throws Exception {
		// get the keys for decryption, derived from masterKeyStr
		byte[][] keys = getKeys(masterKeyStr);
		byte[] hmacKey = keys[0];
		byte[] cipherKey = keys[1];

		// Base64-decode the raw message
		byte[] message = Base64.decodeBase64(encodedMessage.getBytes());

		// capture the last 20 bytes into hmacReceived
		// [[is hmac the checksum? if so, rename hmacReceived to checksum]]
		byte[] hmacReceived = new byte[20];
		System.arraycopy(message, message.length - 20, hmacReceived, 0, 20);

		// capture the rest of the bytes into cipherBytes
		byte[] cipherBytes = new byte[message.length - 20];
		System.arraycopy(message, 0, cipherBytes, 0, cipherBytes.length);

		// use HMACSHA1 to verify the integrity of the message
		Key secKeySpec = new SecretKeySpec(hmacKey, "HMACSHA1");
		Mac hmacObj = Mac.getInstance("HMACSHA1");
		hmacObj.init(secKeySpec);
		hmacObj.update(cipherBytes);
		byte actual[] = hmacObj.doFinal();
		if (actual.length != hmacReceived.length) {
			throw new Exception(
					"Integrity of the message could not be verified");
		}
		for (int i = 0; i < actual.length; i++) {
			if (actual[i] != hmacReceived[i]) {
				throw new GeneralSecurityException("HMAC verification failure");
			}
		}

		// if we made it to here, the raw message is valid; decrypt the message

		// prepare ivSpec for decryption;
		// initialization vector block size is 16 bytes; copy the first
		// 16 bytes of cipherBytes into initVect array
		byte initVect[] = new byte[16];
		System.arraycopy(cipherBytes, 0, initVect, 0, 16);
		IvParameterSpec ivSpec = new IvParameterSpec(initVect);

		// prepare for AES decryption
		SecretKeySpec key = new SecretKeySpec(cipherKey, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		// starting with byte 16 in cipherBytes, decrypt
		cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
		byte[] decryptedBytes = cipher.doFinal(cipherBytes, 16,
				cipherBytes.length - 16);
		String decryptedMessage = new String(decryptedBytes, "UTF-8")
				.toString();

		// for debug
		System.out.println("decryptedMessage:\n" + decryptedMessage + "\n");

		return decryptedMessage;
	}

	/**
	 * Returns hmacKey and cipherKey, derived from masterKeyStr.
	 */
	private static byte[][] getKeys(String masterKeyStr) throws Exception {

		byte[] masterKey = masterKeyStr.getBytes("UTF-8");
		// construct a base hmac ("1" followed by masterKeyStr)
		byte[] baseHmac = concat(
				new byte[] { OpeneBayApplication.HMAC_KEY_LABEL }, masterKey);

		// hash the base hmac; preserve as hmacKey
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(baseHmac);
		byte[] hmacKey = md.digest();

		// construct a base cipher ("0" followed by masterKeyStr)
		byte[] baseCipher = concat(
				new byte[] { OpeneBayApplication.CIPHER_KEY_LABEL }, masterKey);

		// hash the base cipher; preserve the first 16 bytes of this hash as
		// cipherKey
		md.update(baseCipher);

		byte[] hash = md.digest();
		byte[] cipherKey = new byte[16];
		System.arraycopy(hash, 0, cipherKey, 0, 16);

		return new byte[][] { hmacKey, cipherKey };
	}

	private static byte[] concat(byte[] a, byte[] b) {
		byte[] out = new byte[a.length + b.length];
		int cursor = 0;
		System.arraycopy(a, 0, out, cursor, a.length);
		cursor += a.length;
		System.arraycopy(b, 0, out, cursor, b.length);
		return out;
	}

}
