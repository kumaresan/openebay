package controllers.openebay;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 * @author @kumaresan
 * 
 */
public class OpeneBayRequest implements Serializable {

	// Parameters coming from OpeneBay
	public String mid;
	public String lang;
	public String country;
	public String view;
	public String up_dd;
	public String parent;
	public String rpctoken;
	public String env;
	public String globalid;
	public String sp;
	public String st;
	public String sg;
	public String sig;
	public String is;

	public Boolean valideBaySignature;

	// Parameters extracted by decrypting "st"
	public String ted;
	public String at;
	public String un;
	public String es;
	public String t;

	//
	public String openeBayHeaderString;

	public OpeneBayRequest(String mid, String lang, String country,
			String view, String up_dd, String parent, String rpctoken,
			String env, String globalid, String sp, String st, String sg,
			String sig, String is) {
		super();
		this.mid = mid;
		this.lang = lang;
		this.country = country;
		this.view = view;
		this.up_dd = up_dd;
		this.parent = parent;
		this.rpctoken = rpctoken;
		this.env = env;
		this.globalid = globalid;
		this.sp = sp;
		this.st = st;
		this.sg = sg;
		this.sig = sig;
		this.is = is;

		this.valideBaySignature = false;

		this.ted = null;
		this.at = null;
		this.un = null;
		this.es = null;
		this.t = null;

		this.openeBayHeaderString = new String(Base64.decodeBase64(this.is));
		validateRequest();
	}

	private void validateRequest() {
		try {
			this.valideBaySignature = OpeneBayUtil.verifyeBaySignature(
					this.sig, this.sp, this.env);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		try {
			Map<String, String> mapParams = OpeneBayUtil
					.getDecryptedeBayParameters(this.st);
			this.ted = mapParams
					.get(OpeneBayApplication.SECURITY_INFO_TOKEN_EXPIRATION_DATE);
			this.at = mapParams
					.get(OpeneBayApplication.SECURITY_INFO_AUTHTOKEN);
			this.un = mapParams.get(OpeneBayApplication.SECURITY_INFO_USERNAME);
			this.es = mapParams
					.get(OpeneBayApplication.SECURITY_INFO_EIAS_TOKEN);
			this.t = mapParams.get(OpeneBayApplication.SECURITY_INFO_T_NUMBER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
