package controllers.openebay;

import play.Play;
import play.cache.Cache;
import play.mvc.Before;
import play.mvc.Controller;


/**
 * 
 * @author @kumaresan
 *
 */
abstract public class OpeneBayApplication extends Controller {

	public static final String REQ_PARAM_MID = "mid";
	public static final String REQ_PARAM_LANG = "lang";
	public static final String REQ_PARAM_COUNTRY = "country";
	public static final String REQ_PARAM_VIEW = "view";
	public static final String REQ_PARAM_UP_DD = "up_dd";
	public static final String REQ_PARAM_PARENT = "parent";
	public static final String REQ_PARAM_RPCTOKEN = "rpctoken";
	public static final String REQ_PARAM_ENV = "env";
	public static final String REQ_PARAM_GLOBALID = "globalid";
	public static final String REQ_PARAM_SP = "sp";
	public static final String REQ_PARAM_ST = "st";
	public static final String REQ_PARAM_SG = "sg";
	public static final String REQ_PARAM_SIG = "sig";
	public static final String REQ_PARAM_IS = "is";

	// Labels for key derivation
	public static final byte CIPHER_KEY_LABEL = 0;
	public static final byte HMAC_KEY_LABEL = 1;

	public static final String SECURITY_INFO_USERNAME = "un";
	public static final String SECURITY_INFO_AUTHTOKEN = "at";
	public static final String SECURITY_INFO_TOKEN_EXPIRATION_DATE = "ted";
	public static final String SECURITY_INFO_EIAS_TOKEN = "es";
	public static final String SECURITY_INFO_T_NUMBER = "t";

	public static final String OPENEBAY_CANVAS_VIEW_REDIRECT = "openebay.canvasview";
	public static final String OPENEBAY_DEFAULT_VIEW_REDIRECT = "openebay.defaultview";
	public static final String OPENEBAY_WIDE_VIEW_REDIRECT = "openebay.wideview";

	public static final String OPENEBAY_DEV_ID = "openebay.devid";
	public static final String OPENEBAY_APP_ID = "openebay.appid";
	public static final String OPENEBAY_CERT_ID = "openebay.certid";

	public static final String OPENEBAYREQUEST = "openebayrequest";

	@Before(unless = { "canvasView", "defaultView" })
	public static void checkOpeneBayRequest() throws Throwable {
		OpeneBayRequest openeBayRequest = getOpeneBayRequest();
		if (openeBayRequest == null) {
			forbidden("Session expired. Please login again");
		}
	}

	private static void processOpeneBayRequest(String mid, String lang,
			String country, String view, String up_dd, String parent,
			String rpctoken, String env, String globalid, String sp, String st,
			String sg, String sig, String is) {
		OpeneBayRequest openebayrequest = new OpeneBayRequest(mid, lang,
				country, view, up_dd, parent, rpctoken, env, globalid, sp, st,
				sg, sig, is);
		setOpeneBayRequest(openebayrequest);
	}

	private static void setOpeneBayRequest(OpeneBayRequest openebayrequest) {
		renderArgs.put(OPENEBAYREQUEST, openebayrequest);
		Cache.set(OPENEBAYREQUEST + "_" + session.getId(), openebayrequest);
	}

	public static OpeneBayRequest getOpeneBayRequest() {
		OpeneBayRequest openebayrequest = (OpeneBayRequest) renderArgs
				.get(OPENEBAYREQUEST);
		System.out.println("OpeneBayApplication.getOpeneBayRequest() 1 "
				+ openebayrequest);
		if (openebayrequest == null) {
			openebayrequest = Cache.get(
					OPENEBAYREQUEST + "_" + session.getId(),
					OpeneBayRequest.class);
			System.out.println("OpeneBayApplication.getOpeneBayRequest() 2 "
					+ openebayrequest);
		}
		return openebayrequest;
	}

	public static void canvasView(String mid, String lang, String country,
			String view, String up_dd, String parent, String rpctoken,
			String env, String globalid, String sp, String st, String sg,
			String sig, String is) {
		
		if (mid == null) {
			flash.error("Invalid Open eBay request. Check Logs for details");
			render();
		}

		processOpeneBayRequest(mid, lang, country, view, up_dd, parent,
				rpctoken, env, globalid, sp, st, sg, sig, is);
		final String redirectTo = Play.configuration
				.getProperty(OPENEBAY_CANVAS_VIEW_REDIRECT);
		if (redirectTo != null) {
			redirect(redirectTo);
		}

		flash.success("Successfully Parsed Open eBay Request");

		render();
	}

	public static void defaultView(String mid, String lang, String country,
			String view, String up_dd, String parent, String rpctoken,
			String env, String globalid, String sp, String st, String sg,
			String sig, String is) {
		processOpeneBayRequest(mid, lang, country, view, up_dd, parent,
				rpctoken, env, globalid, sp, st, sg, sig, is);

		final String redirectTo = Play.configuration
				.getProperty(OPENEBAY_DEFAULT_VIEW_REDIRECT);
		if (redirectTo != null) {
			redirect(redirectTo);
		}

		render();
	}

	public static void wideView(String mid, String lang, String country,
			String view, String up_dd, String parent, String rpctoken,
			String env, String globalid, String sp, String st, String sg,
			String sig, String is) {

		processOpeneBayRequest(mid, lang, country, view, up_dd, parent,
				rpctoken, env, globalid, sp, st, sg, sig, is);

		final String redirectTo = Play.configuration
				.getProperty(OPENEBAY_WIDE_VIEW_REDIRECT);
		if (redirectTo != null) {
			redirect(redirectTo);
		}

		render();
	}

}
